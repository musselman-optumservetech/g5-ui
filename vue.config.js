// const BundleAnalyzerPlugin = require('webpack-bundle-analyzer')
//     .BundleAnalyzerPlugin;

module.exports = {
    publicPath: '/',
    "transpileDependencies": [
        "vuetify"
    ],

    devServer: {
        port: process.env.VUE_APP_PORT
    },

    // configureWebpack: {
    //   plugins: [new BundleAnalyzerPlugin()]
    // },

    pluginOptions: {
        i18n: {
            locale: 'en',
            fallbackLocale: 'en',
            localeDir: 'locales',
            enableInSFC: false
        }
    }
}