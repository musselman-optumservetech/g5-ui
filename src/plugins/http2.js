import axios from 'axios';

const instance = axios.create({
  baseURL: process.env.VUE_APP_API_PATH2,
  headers: {
    common: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }
});

export default instance;