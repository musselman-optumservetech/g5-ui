import Vue from 'vue';
import transform from 'lodash/transform';
import isEqual from 'lodash/isEqual';
import isArray from 'lodash/isArray';
import isObject from 'lodash/isObject';


Vue.mixin({
    methods: {
        difference(origObj, newObj) {
            function changes(newObj, origObj) {
                let arrayIndexCounter = 0
                return transform(newObj, function (result, value, key) {
                    if (!isEqual(value, origObj[key])) {
                        let resultKey = isArray(origObj) ? arrayIndexCounter++ : key
                        result[resultKey] = (isObject(value) && isObject(origObj[key])) ? changes(value, origObj[key]) : { original: origObj[key], updated: value };
                    }
                })
            }
            return changes(newObj, origObj)
        }
    }
})