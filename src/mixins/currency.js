import Vue from 'vue';
import isEmpty from 'lodash/isEmpty';

Vue.mixin({
    methods: {
        convertToFloat(value) {
            value = value != undefined && value != "" ? value : 0.00;
            return value.toString().replace(/,/g, '');
        },
        convertToCurrency(value) {
            let n = value.toString().replace(/,/g, '');
            n = parseFloat(n).toFixed(2);
            n = Number(n).toLocaleString('en', { minimumFractionDigits: 2, maximumFractionDigits: 2 });
            return n;
        },
        getPeriodRequestedTotal(period) {
            let total = 0;
            total += parseFloat(this.convertToFloat(period.personnelReqFedAmt));
            total += parseFloat(this.convertToFloat(period.fringeReqFedAmt));
            total += parseFloat(this.convertToFloat(period.travelReqFedAmt));
            total += parseFloat(this.convertToFloat(period.equipReqFedAmt));
            total += parseFloat(this.convertToFloat(period.supplyReqFedAmt));
            total += parseFloat(this.convertToFloat(period.contractReqFedAmt));
            total += parseFloat(this.convertToFloat(period.constructReqFedAmt));
            total += parseFloat(this.convertToFloat(period.otherReqFedAmt));
            total += parseFloat(this.convertToFloat(period.trainingReqFedAmt));
            total += parseFloat(this.convertToFloat(period.indirectReqFedAmt));
            return total;
        },
        getPeriodRecommendedTotal(period) {
            let total = 0;
            total += parseFloat(this.convertToFloat(period.personnelRecFedAmt));
            total += parseFloat(this.convertToFloat(period.fringeRecFedAmt));
            total += parseFloat(this.convertToFloat(period.travelRecFedAmt));
            total += parseFloat(this.convertToFloat(period.equipRecFedAmt));
            total += parseFloat(this.convertToFloat(period.supplyRecFedAmt));
            total += parseFloat(this.convertToFloat(period.contractRecFedAmt));
            total += parseFloat(this.convertToFloat(period.constructRecFedAmt));
            total += parseFloat(this.convertToFloat(period.otherRecFedAmt));
            total += parseFloat(this.convertToFloat(period.trainingRecFedAmt));
            total += parseFloat(this.convertToFloat(period.indirectRecFedAmt));
            return total;
        },
        getPeriodNonTotal(period) {
            let total = 0;
            total += parseFloat(this.convertToFloat(period.personnelNonFedAmt));
            total += parseFloat(this.convertToFloat(period.fringeNonFedAmt));
            total += parseFloat(this.convertToFloat(period.travelNonFedAmt));
            total += parseFloat(this.convertToFloat(period.equipNonFedAmt));
            total += parseFloat(this.convertToFloat(period.supplyNonFedAmt));
            total += parseFloat(this.convertToFloat(period.contractNonFedAmt));
            total += parseFloat(this.convertToFloat(period.constructNonFedAmt));
            total += parseFloat(this.convertToFloat(period.otherNonFedAmt));
            total += parseFloat(this.convertToFloat(period.trainingNonFedAmt));
            total += parseFloat(this.convertToFloat(period.indirectNonFedAmt));
            return total;
        },
        getFloatTotalRequestedAmount(applicant) {
            let total = 0.00;
            if(isEmpty(applicant)) return total;
            if(isEmpty(applicant.budgetEntities)) return total;
            let current = applicant.budgetEntities.filter( period => period.budgetStatusCd === "CU");
            if (!isEmpty(current)) {
                total = this.getPeriodRequestedTotal(current[0]);
            }
            return total;
        },
        getTotalRequestedAmountForPeriod(applicant) {
            let total = 0.00;
            if(isEmpty(applicant)) return total;
            if(isEmpty(applicant.budgetEntities)) return total;
            applicant.budgetEntities.forEach( period => {
                total += this.getPeriodRequestedTotal(period)
            })
            return this.convertToCurrency(total);
        },
        getTotalRequestedAmount(applicant) {
            let total = 0.00;
            if(isEmpty(applicant)) return total;
            if(isEmpty(applicant.budgetEntities)) return total;
            let current = applicant.budgetEntities.filter( period => period.budgetStatusCd === "CU");
            if (!isEmpty(current)) {
                total = this.getPeriodRequestedTotal(current[0]);
            }
            return this.convertToCurrency(total);
        },
        getTotalRecommendedAmount(applicant) {
            let total = 0.00;
            if(isEmpty(applicant)) return total;
            if(isEmpty(applicant.budgetEntities)) return total;
            let current = applicant.budgetEntities.filter( period => period.budgetStatusCd === "CU");
            if (!isEmpty(current)) {
                total = this.getPeriodRecommendedTotal(current[0]);
            }

            return this.convertToCurrency(total);
        },
        convertForUpload(period) {
            period.personnelReqFedAmt = this.convertToFloat(period.personnelReqFedAmt);
            period.fringeReqFedAmt = this.convertToFloat(period.fringeReqFedAmt);
            period.travelReqFedAmt = this.convertToFloat(period.travelReqFedAmt);
            period.equipReqFedAmt = this.convertToFloat(period.equipReqFedAmt);
            period.supplyReqFedAmt = this.convertToFloat(period.supplyReqFedAmt);
            period.contractReqFedAmt = this.convertToFloat(period.contractReqFedAmt);
            period.constructReqFedAmt = this.convertToFloat(period.constructReqFedAmt);
            period.otherReqFedAmt = this.convertToFloat(period.otherReqFedAmt);
            period.trainingReqFedAmt = this.convertToFloat(period.trainingReqFedAmt);
            period.indirectReqFedAmt = this.convertToFloat(period.indirectReqFedAmt);
            period.personnelRecFedAmt = this.convertToFloat(period.personnelRecFedAmt);
            period.fringeRecFedAmt = this.convertToFloat(period.fringeRecFedAmt);
            period.travelRecFedAmt = this.convertToFloat(period.travelRecFedAmt);
            period.equipRecFedAmt = this.convertToFloat(period.equipRecFedAmt);
            period.supplyRecFedAmt = this.convertToFloat(period.supplyRecFedAmt);
            period.contractRecFedAmt = this.convertToFloat(period.contractRecFedAmt);
            period.constructRecFedAmt = this.convertToFloat(period.constructRecFedAmt);
            period.otherRecFedAmt = this.convertToFloat(period.otherRecFedAmt);
            period.trainingRecFedAmt = this.convertToFloat(period.trainingRecFedAmt);
            period.indirectRecFedAmt = this.convertToFloat(period.indirectRecFedAmt);
            period.personnelNonFedAmt = this.convertToFloat(period.personnelNonFedAmt);
            period.fringeNonFedAmt = this.convertToFloat(period.fringeNonFedAmt);
            period.travelNonFedAmt = this.convertToFloat(period.travelNonFedAmt);
            period.equipNonFedAmt = this.convertToFloat(period.equipNonFedAmt);
            period.supplyNonFedAmt = this.convertToFloat(period.supplyNonFedAmt);
            period.contractNonFedAmt = this.convertToFloat(period.contractNonFedAmt);
            period.constructNonFedAmt = this.convertToFloat(period.constructNonFedAmt);
            period.otherNonFedAmt = this.convertToFloat(period.otherNonFedAmt);
            period.trainingNonFedAmt = this.convertToFloat(period.trainingNonFedAmt);
            period.indirectNonFedAmt = this.convertToFloat(period.indirectNonFedAmt);
            return period;
        },
        convertForViewing(period) {
            period.personnelReqFedAmt = this.convertToCurrency(this.convertToFloat(period.personnelReqFedAmt));
            period.fringeReqFedAmt = this.convertToCurrency(this.convertToFloat(period.fringeReqFedAmt));
            period.travelReqFedAmt = this.convertToCurrency(this.convertToFloat(period.travelReqFedAmt));
            period.equipReqFedAmt = this.convertToCurrency(this.convertToFloat(period.equipReqFedAmt));
            period.supplyReqFedAmt = this.convertToCurrency(this.convertToFloat(period.supplyReqFedAmt));
            period.contractReqFedAmt = this.convertToCurrency(this.convertToFloat(period.contractReqFedAmt));
            period.constructReqFedAmt = this.convertToCurrency(this.convertToFloat(period.constructReqFedAmt));
            period.otherReqFedAmt = this.convertToCurrency(this.convertToFloat(period.otherReqFedAmt));
            period.trainingReqFedAmt = this.convertToCurrency(this.convertToFloat(period.trainingReqFedAmt));
            period.indirectReqFedAmt = this.convertToCurrency(this.convertToFloat(period.indirectReqFedAmt));
            period.personnelRecFedAmt = this.convertToCurrency(this.convertToFloat(period.personnelRecFedAmt));
            period.fringeRecFedAmt = this.convertToCurrency(this.convertToFloat(period.fringeRecFedAmt));
            period.travelRecFedAmt = this.convertToCurrency(this.convertToFloat(period.travelRecFedAmt));
            period.equipRecFedAmt = this.convertToCurrency(this.convertToFloat(period.equipRecFedAmt));
            period.supplyRecFedAmt = this.convertToCurrency(this.convertToFloat(period.supplyRecFedAmt));
            period.contractRecFedAmt = this.convertToCurrency(this.convertToFloat(period.contractRecFedAmt));
            period.constructRecFedAmt = this.convertToCurrency(this.convertToFloat(period.constructRecFedAmt));
            period.otherRecFedAmt = this.convertToCurrency(this.convertToFloat(period.otherRecFedAmt));
            period.trainingRecFedAmt = this.convertToCurrency(this.convertToFloat(period.trainingRecFedAmt));
            period.indirectRecFedAmt = this.convertToCurrency(this.convertToFloat(period.indirectRecFedAmt));
            period.personnelNonFedAmt = this.convertToCurrency(this.convertToFloat(period.personnelNonFedAmt));
            period.fringeNonFedAmt = this.convertToCurrency(this.convertToFloat(period.fringeNonFedAmt));
            period.travelNonFedAmt = this.convertToCurrency(this.convertToFloat(period.travelNonFedAmt));
            period.equipNonFedAmt = this.convertToCurrency(this.convertToFloat(period.equipNonFedAmt));
            period.supplyNonFedAmt = this.convertToCurrency(this.convertToFloat(period.supplyNonFedAmt));
            period.contractNonFedAmt = this.convertToCurrency(this.convertToFloat(period.contractNonFedAmt));
            period.constructNonFedAmt = this.convertToCurrency(this.convertToFloat(period.constructNonFedAmt));
            period.otherNonFedAmt = this.convertToCurrency(this.convertToFloat(period.otherNonFedAmt));
            period.trainingNonFedAmt = this.convertToCurrency(this.convertToFloat(period.trainingNonFedAmt));
            period.indirectNonFedAmt = this.convertToCurrency(this.convertToFloat(period.indirectNonFedAmt));
            return period;
        }
    }
})