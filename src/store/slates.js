import http from '../plugins/http';
import http2 from '../plugins/http2';
import http3 from '../plugins/http3';
import http4 from '../plugins/http4';
import cloneDeep from 'lodash/cloneDeep';

const state = {
  slate: {
      "fiscalYear": "2020",
      "subProgram": "84.016A",
      "scheduleNo": "1",
      "contact": "John Smith",
      "actionType": "New Competition",
      "scheduleTitle": "Undergraduate International Studies",
      "plannedAmount": 2500000,
      "performancePeriod": 60
  },
  applications: [],
  applicant: {},
  modifiedApplicant: {},
  narratives: [],
  newBudget: {
    budgetPerdCompleteInd: "N",
    budgetPerdEndDate: "",
    budgetPerdFy: 2021,
    budgetPerdId: undefined,
    budgetPerdSeq: 1,
    budgetPerdStartDate: "",
    budgetStatusCd: "CU",
    constructNonFedAmt: 0.00,
    constructRecFedAmt: 0.00,
    constructReqFedAmt: 0.00,
    contractNonFedAmt: 0.00,
    contractRecFedAmt: 0.00,
    contractReqFedAmt: 0.00,
    equipNonFedAmt: 0.00,
    equipRecFedAmt: 0.00,
    equipReqFedAmt: 0.00,
    fapiisCertificateInd: "N",
    fringeNonFedAmt: 0.00,
    fringeRecFedAmt: 0.00,
    fringeReqFedAmt: 0.00,
    indirectNonFedAmt: 0.00,
    indirectRecFedAmt: 0.00,
    indirectReqFedAmt: 0.00,
    keyPersonnelInfoEntities: [],
    otherNonFedAmt: 0.00,
    otherRecFedAmt: 0.00,
    otherReqFedAmt: 0.00,
    participantDecimal: 0.00,
    personnelNonFedAmt: 0.00,
    personnelRecFedAmt: 0.00,
    personnelReqFedAmt: 0.00,
    samCertificateInd: "N",
    supplyNonFedAmt: 0.00,
    supplyRecFedAmt: 0.00,
    supplyReqFedAmt: 0.00,
    trainingNonFedAmt: 0.00,
    trainingRecFedAmt: 0.00,
    trainingReqFedAmt: 0.00,
    travelNonFedAmt: 0.00,
    travelRecFedAmt: 0.00,
    travelReqFedAmt: 0.00
  },
  period: {}
};

const getters = {
    getSlate: state => {
      return state.slate
    },
    getApplications: state => {
      return state.applications;
    },
    getApplicant: state => {
      return state.applicant;
    },
    getModifiedApplicant: state => {
      return state.modifiedApplicant;
    },
    getNarratives: state => {
      return state.narratives;
    },
    getNewBudget: state => {
      return state.newBudget;
    },
    getPeriod: state => {
      return state.period;
    }
};

const mutations = {
    setSlate(state, response) {
      state.slate = response;
    },
    setApplications(state, response) {
      state.applications = response;
    },
    updateSlateApplication(state, obj) {
      state.applications.forEach(application => {
        if (application.applicationId === obj.id) {
          application[obj.key] = obj.value;
        }
      });
    },
    setApplicant(state, response) {
      state.applicant = response;
    },
    setModifiedApplicant(state, response) {
      state.modifiedApplicant = response;
    },
    updateApplicant(state, obj) {
      state.modifiedApplicant[obj.key] = obj.value;
    },
    updateOriginal(state, obj) {
      state.applicant[obj.key] = obj.value;
    },
    setNarratives(state, response) {
      state.narratives = response;
    },
    setPeriod(state, response) {
      state.period = cloneDeep(response);
    },
    updatePeriod(state, obj) {
      state.period[obj.key] = obj.value;
    }
};

const actions = {
    async getApplications (context) {
      const response = await http.get('return-applications');
      context.commit('setApplications', response.data);
    },
    async getApplicant (context, data) {
      const response = await http.get('return-application/' + data);
      context.commit('setApplicant', cloneDeep(response.data));
      context.commit('setModifiedApplicant', cloneDeep(response.data));
    },
    async updateApplication(context, data) {
      let applicant = cloneDeep(data);
      applicant.budgetEntities = [];
      const response = await http.put('process-application/' + data.applicationId, applicant);
      context.commit('setApplicant', cloneDeep(response.data));
      context.commit('setModifiedApplicant', cloneDeep(response.data));
      return response.data;
    },
    async addBudget(context, data) {
      const response = await http3.post('add-budget', data);
      return response.data;
    },
    async updateBudget(context, data) {
      const response = await http3.put('update-single-budget/', data);
      return response.data;
    },
    async removeBudget(context, data) {
      const response = await http3.delete('remove-budget/' + data.budgetPerdId);
      return response.data;
    },
    async getNarratives(context) {
      const response = await http.get('return-all-narratives');
      context.commit('setNarratives', response.data);
    },
    async addAbstract(context, data) {
      const response = await http2.post('add-abstract/', data);
      return response.data;
    },
    async removeAbstract(context, data) {
      console.log("Abstract: " + JSON.stringify(data));
      const response = await http2.delete('remove-abstract/' + data.abstractId, data);
      return response.data;
    },
    async approveApplication(context, data) {
      await http4.put('process-slate/' + data.applicationId, data);
    }
};

export default {
    namespaced: true,
    state,
    getters,
    mutations,
    actions
  };