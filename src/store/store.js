import Vue from 'vue';
import Vuex from 'vuex';

import params from './params';
import slates from './slates';

Vue.use(Vuex);

export const store = new Vuex.Store({
  modules: {
    params,
    slates
  }
});