const state = {
  props: {
    narrativeReceivedInd: {
      locale: "application.narrative"
    },
    applicationCompleteInd: {
      locale: "application.appComplete"
    },
    lateInd: {
      locale: "application.appLate"
    },
    grantFundingStatusCd: {
      locale: "applicant.grantFundingStatusCd",
      required: false,
      hideDetails: true,
      noLabel: true,
      data: [
        {  Key: "RR", Value: "Recommended" },
        {  Key: "PR", Value: "Printed" },
        {  Key: "CO", Value: "Committed" },
        {  Key: "NR", Value: "Not Recommended" },
        {  Key: "OB", Value: "Obligated" },
        {  Key: "OH", Value: "On-hold" },
        {  Key: "AP", Value: "Approved", disabled: true },
        {  Key: "RC", Value: "Received" },
        {  Key: "SM", Value: "Submitted" },
        {  Key: "RJ", Value: "Rejected", disabled: true },
        {  Key: "PC", Value: "Pending Commitment" },
        {  Key: "DC", Value: "Decommitted" },
        {  Key: "CP", Value: "Printed" },
        {  Key: "PO", Value: "Pending Obligation" },
        {  Key: "RI", Value: "Reinstatment Initiated" },
        {  Key: "MV", Value: "Moved" },
        {  Key: "US", Value: "Unsubmitted" },
        {  Key: "RO", Value: "Rolled Over" },
        {  Key: "DE", Value: "Deleted" }
      ],
      value: "Key",
      text: "Value",
      clearable: false
    },
    edProgramContact: {
      locale: "applicant.edProgramContact",
      required: false,
      noLabel: true,
      hideDetails: true,
      data: [
        { Key: "David Johnson", value: "David Johnson" },
        { Key: "Richard Hill", value: "Richard Hill" },
        { Key: "Sarah Jackson", value: "Sarah Jackson" },
        { Key: "Sterling Walt", value: "Sterling Walt" },
        { Key: "Stone Hill", value: "Stone Hill" }
      ],
      value: "Key",
      text: "value",
      clearable: false
    },
    applicationComment: {
      locale: "application.comment",
      required: false
    },
    appComplete: {
      locale: "application.appComplete",
      required: false
    },
    appLate: {
      locale: "application.appLate",
      required: false
    },
    appDuplicate: {
      locale: "application.appDuplicate",
      required: false
    },
    dupConfirmed: {
      locale: "application.dupConfirmed",
      required: false
    },
    duplicateAward: {
      locale: "application.dupAwardNo",
      required: false
    },
    eligibleStatusCd: {
      locale: "application.eligibilityStatus",
      required: true,
      data: [
        { Key: "EL", value: " Eligible" },
        { Key: "IE", value: "Ineligible" },
        { Key: "PD", value: "Pending" }
      ],
      value: "Key",
      text: "value",
      persistentHint: true
    },
    ineligibleCatCd: {
      locale: "application.ineligibleReason",
      required: true,
      data: [
        { Key: "NA", value: "Not Applicable" },
        { Key: "NE", value: "The Application is not eligible" },
        { Key: "NC", value: "The Applicant does not comply with all of the Procedural Rules that govern the submission of the Application" },
        { Key: "NI", value: "The Application does not contain the information required under the program" },
        { Key: "NP", value: "The proposed project cannot be funded under the authorizing statute or implementing regulations for the program" },
      ],
      value: "Key",
      text: "value",
      persistentHint: true
    },
    eligibilityStatusCmt: {
      locale: "application.ineligibleComments",
      required: true
    },
    budgetPerdFy: {
      locale: "budget.year",
      required: true,
      format: "####",
      placeHolder: "YYYY",
      noLabel: true,
      hideDetails: true,
    },
    budgetPerdStartDate: {
      locale: "budget.startDate",
      required: true,
      format: "##/##/####",
      placeHolder: "MM/DD/YYYY",
      noLabel: true,
      hideDetails: true,
    },
    budgetPerdEndDate: {
      locale: "budget.endDate",
      required: true,
      format: "##/##/####",
      placeHolder: "MM/DD/YYYY",
      noLabel: true,
      hideDetails: true,
    },
    currentPeriod: {
      locale: "period.currentPeriod",
      required: true,
      value: "key",
      text: "value",
      hideDetails: true
    },
    personnelReqFedAmt: {
      locale: "period.personnel",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    fringeReqFedAmt: {
      locale: "period.fringe",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    travelReqFedAmt: {
      locale: "period.travel",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    equipReqFedAmt: {
      locale: "period.equip",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    supplyReqFedAmt: {
      locale: "period.supply",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    contractReqFedAmt: {
      locale: "period.contract",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    constructReqFedAmt: {
      locale: "period.construct",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    otherReqFedAmt: {
      locale: "period.other",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    trainingReqFedAmt: {
      locale: "period.training",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    indirectReqFedAmt: {
      locale: "period.indirect",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    supplementReqFedAmt: {
      locale: "period.supplement",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    personnelRecFedAmt: {
      locale: "period.personnel",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    fringeRecFedAmt: {
      locale: "period.fringe",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    travelRecFedAmt: {
      locale: "period.travel",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    equipRecFedAmt: {
      locale: "period.equip",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    supplyRecFedAmt: {
      locale: "period.supply",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    contractRecFedAmt: {
      locale: "period.contract",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    constructRecFedAmt: {
      locale: "period.construct",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    otherRecFedAmt: {
      locale: "period.other",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    trainingRecFedAmt: {
      locale: "period.training",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    indirectRecFedAmt: {
      locale: "period.indirect",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    supplementRecFedAmt: {
      locale: "period.supplement",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    personnelNonFedAmt: {
      locale: "period.personnel",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    fringeNonFedAmt: {
      locale: "period.fringe",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    travelNonFedAmt: {
      locale: "period.travel",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    equipNonFedAmt: {
      locale: "period.equip",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    supplyNonFedAmt: {
      locale: "period.supply",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    contractNonFedAmt: {
      locale: "period.contract",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    constructNonFedAmt: {
      locale: "period.construct",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    otherNonFedAmt: {
      locale: "period.other",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    trainingNonFedAmt: {
      locale: "period.training",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    indirectNonFedAmt: {
      locale: "period.indirect",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    supplementNonFedAmt: {
      locale: "period.supplement",
      required: false,
      noLabel: true,
      hideDetails: true,
      isMoney: true
    },
    firstName: {
      locale: "personnel.firstName",
      required: true,
    },
    middleInitial: {
      locale: "personnel.middleInital",
      required: false,
    },
    lastName: {
      locale: "personnel.lastName",
      required: true,
    },
    phoneNumber: {
      locale: "personnel.phoneNumber",
      required: true,
    },
    email: {
      locale: "personnel.email",
      required: true,
    },
    levelOfEffort: {
      locale: "personnel.levelOfEffort",
      required: true,
    },
    keyPersonnelTitleCd: {
      locale: "personnel.title",
      required: false,
      data: [
        { Key: "01", value: "Project Director" },
        { Key: "02", value: "Principal Investigator" },
        { Key: "03", value: "Project Manager" },
        { Key: "04", value: "Director" },
        { Key: "05", value: "Executive Director" },
        { Key: "06", value: "State Director" },
        { Key: "07", value: "Certifying Official" },
        { Key: "08", value: "Co Project Maanger" }
      ],
      value: "Key",
      text: "value",
    },
    currentlySelectedBudget: {
      locale: "budget.currentSelected",
      value: "Key",
      text: "value",
      required: true
    },
    comments: {
      locale: "abstract.comments",
      required: true
    },
    budgetStatusCd: {
      locale: "budget.budgetStatusCd",
      value: "Key",
      text: "value",
      data: [
        { Key: "CU", value: "Current" },
        { Key: "DE", value: "Deleted" },
        { Key: "FU", value: "Future" },
        { Key: "PR", value: "Previous" }
      ]
    },
    budgetEntities: {
      locale: "budgetEntities"
    }
  }
};

const getters = {
  getProps: state => {
    return state.props;
  }
}

export default {
  namespaced: true,
  state,
  getters
};