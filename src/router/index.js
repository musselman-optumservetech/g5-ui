import Vue from 'vue';
import Router from 'vue-router';
import Vuetify from 'vuetify';

import 'vuetify/dist/vuetify.min.css'

Vue.use(Router)
Vue.use(Vuetify)

const router = new Router({
  base: '/',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'slates',
      component: () => import( /* webpackChunkName: "slates" */ '../views/Slates.vue'),
      meta: {
        title: "Slates"
      }
    },
    {
      path: '/applicant',
      name: 'applicant',
      component: () => import( /* webpackChunkName: "applicant" */ '../views/Applicant.vue'),
      meta: {
        title: "Applicant"
      },
      children: [
        {
          path: '/applicant/budget/:id',
          name: 'budget',
          component: () => import( /* webpackChunkName: "budget" */ '../views/Budget.vue'),
          meta: {
            title: "Budget"
          }
        },
        {
          path: '/applicant/application/:id',
          name: 'application',
          component: () => import( /* webpackChunkName: "application" */ '../views/Application.vue'),
          meta: {
            title: "Application"
          }
        },
        {
          path: '/applicant/abstract/:id',
          name: 'abstract',
          component: () => import( /* webpackChunkName: "abstract" */ '../views/Abstract.vue'),
          meta: {
            title: "Abstract"
          }
        },
      ]
    }
  ]
})

export default router;