import 'babel-polyfill';
import 'core-js/stable';
import 'regenerator-runtime/runtime';
import '@/mixins/currency';
import '@/mixins/difference';
import Vue from 'vue'
import App from './App.vue'
import { store } from './store/store';
import router from './router'
import vuetify from './plugins/vuetify';
import i18n from './plugins/i18n'
import http from './plugins/http';
import { TextEncoder } from 'text-encoding';
import 'webcrypto-shim';
import VueMask from 'v-mask';

Vue.config.productionTip = false;
Vue.prototype.$http = http;
Vue.use(VueMask);

if (typeof window.TextEncoder === 'undefined') {
  window.TextEncoder = TextEncoder;
}

new Vue({
  store,
  router,
  vuetify,
  i18n,
  render: h => h(App)
}).$mount('#app')
